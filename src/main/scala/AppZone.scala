import schema.PetaSkySchema
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

object AppZone {
  /*     */
  //case class Case(id:Int,zone:Zone){}
  case class Zone(minRA: Double, maxRA: Double,minDecl: Double, maxDecl: Double) {
      def zone_englobante(b: Zone): Zone = {
        // calcule une zone qui englobe this et b
        Zone(Math.min(minRA,b.minRA),
             Math.max(maxRA,b.maxRA),
             Math.min(minDecl,b.minDecl),
             Math.max(maxDecl,b.maxDecl))
      }
    /* generation d'un tuple  */
      def toTuple: (Double, Double, Double, Double) = {
        (minRA, maxRA, minDecl, maxDecl)
      }
    }
  /* ici on construit un Array de zone pour tous les fichiers pour pouvoir construire le ciel */
  def skyConctrution(sc:SparkContext,nameFolder:String):Zone={
    sc.textFile(nameFolder+"/*").cache()
      .map(_.split(",").map(_.trim))
      .map(toZone).reduce((a,b) => a.zone_englobante(b))
  }
  /**
    * ici on transforme un array a une zone
    * @param input
    * @return
    */
  def toZone(input:Array[String]):Zone={
    val petaSky = PetaSkySchema
    Zone(input(petaSky.s_ra).toDouble,
      input(petaSky.s_ra).toDouble,input(petaSky.s_decl).toDouble,input(petaSky.s_decl).toDouble)
  }

  /* decoupage de ciel en zone   */
  /**
    *
    * @param ciel
    * @param paquetSize
    * @return
    */
  def splitSky(ciel: Zone,paquetSize:Int):Array[(Int,Double,Double,Double,Double)] = {
    val longueur = ciel.maxRA -ciel.minRA
    val largeur = ciel.maxDecl - ciel.minDecl
    val nbZones = Math.sqrt((longueur*largeur)/paquetSize).toInt
    var result:Array[(Int,Double,Double,Double,Double)] = new Array[(Int,Double,Double,Double,Double)](Math.pow(nbZones+1,2).toInt)
    //println(s"nb zones est : $nbZones")
    val intervalleSurRa = (longueur/nbZones)
    val integralleSurDecl = (largeur/nbZones)
    // ici c'est la construction des zones
    var i:Int = 0
    var j:Int = 0
    var count:Int =0
    var Xrep:Double = ciel.minRA -0.5 // on ajoute epsilon = 0.5 pour eviter prendre en compte les points sur les bordures du ciel
    var Yrep:Double = ciel.maxDecl +0.5
    for (i <- 0 to nbZones){
      for(j <- 0 to nbZones){
        result(count) = (count,Xrep,Xrep+intervalleSurRa,Yrep-integralleSurDecl,Yrep)
        Xrep += intervalleSurRa
        count+=1
      }
      Xrep = ciel.minRA -0.5
      Yrep -= integralleSurDecl
    }

    result
  }

  /**
    * ici on stocke les informations sur les zones dans un fichier qu'on utilise comme catalogue
    * @param input
    * @param url
    * @param sc
    */
  def StockDtatOfZones(input:Array[(Int,Double,Double,Double,Double)],url:String,sc:SparkContext): Unit ={
    //TIW6RDDUtils.writePairRDDToHadoopUsingKeyAsFileName(input.)
    val myRDD = sc.parallelize(input.toList)
    myRDD.map(r => "%d,%s,%s,%s,%s".format(r._1,r._2,r._3,r._4,r._5)).
      saveAsTextFile(url)
  }

  /**
    *  c'est la fonction qui charge le catalogue
     * @param url
    * @param sc
    * @return
    */
  def LireCatalogue(url:String,sc:SparkContext):Array[(Int,Double,Double,Double,Double)]={
    sc.textFile(url).cache().map(_.split(",").map(_.trim)).map(r => (r(0).toInt,r(1).toDouble,r(2).toDouble,r(3).toDouble,r(4).toDouble)).collect()
  }


  /**
    * ici on return une Map qui associe a chaque tuple des data la zone correspondante
    * @param input c'est un tuple des data Source
    * @param catalog catalogue
    * @return une map contient zone => tuple
    */
  def pointAppartenance(input:Array[String],catalog :Array[(Int,Double,Double,Double,Double)]): Map[Int,String] ={
    val petaSky = PetaSkySchema
    var maps = Map[Int,String]()
    for(ele <- catalog){
      if (input(petaSky.s_ra ).toDouble >= ele._2
        && input(petaSky.s_ra).toDouble <= ele._3
        && input(petaSky.s_decl).toDouble >= ele._4
        && input(petaSky.s_decl).toDouble <= ele._5) {

          return Map( ele._1 -> input.mkString(","))
      }
    }
    Map(-1 -> null)
  }
  /**
    * c'est une fonction qui sert a partitionner les data selon les zone
    * @param sc : SparkContext pour retourner un RDD
    * @param urlFolder correspond au path du repertoire qui contient les data
    * @param catalog le catalogue des zones
    * @return un rdd de zone => tuple
    */
  def partitionnementFichiers(sc:SparkContext,urlFolder:String,catalog:Array[(Int,Double,Double,Double,Double)]):RDD[(String,String)]={
    sc.textFile(urlFolder).cache().map(_.split(",").map(_.trim)).map(r => {
       val pAppart = pointAppartenance(r,catalog)

      ("zone_"+pAppart.keySet.head.toString,pAppart.values.head)
    })
  }

  /**
    * une fonction qui affiche dans quelle zone situent les voisins du point entree en paramètre
    * @param point
    * @param urlToCatalog
    * @param sc
    */
  def pointNeighboursZone(point:Array[String],urlToCatalog:String,sc:SparkContext): Unit ={
    val petaSky = PetaSkySchema
    val catalog = LireCatalogue(urlToCatalog,sc)
    var maps = Map[Int,String]()
    for(ele <- catalog){
      if (point(petaSky.s_ra ).toDouble >= ele._2
        && point(petaSky.s_ra).toDouble <= ele._3
        && point(petaSky.s_decl).toDouble >= ele._4
        && point(petaSky.s_decl).toDouble <= ele._5) {

        println(s"Le voisinnage de cd point appartient a la zone ${ele._1}")
      }
    }
    println("Ce point est en dehors du ciel")
  }
}

