import org.apache.spark.{SparkConf, SparkContext}

object AppFilesPartition {
  def main (args: Array[String]): Unit = {
    val compte = "p1608626" // à modifier selon l'utilisateur
    if (args.length == 3) {
      val conf = new SparkConf().setAppName( "AppFilesPartition" )
      val sc = new SparkContext( conf )
      val catalog = AppZone.LireCatalogue(args(0),sc)
      val rdd = AppZone.partitionnementFichiers( sc, args(1) ,catalog)
      TIW6RDDUtils.writePairRDDToHadoopUsingKeyAsFileName(rdd, args(2), catalog.length)
      sc.stop()
    }else {
      println("Usage: spark-submit --class AppFilesPartition /home/" + compte + "/TPNote-assembly-1.0.jar " +
        "hdfs:///path-to-catalogue hdfs:///tp-data/Source"+
        "[hdfs:///user/" + compte + "/repertoire-resultat]")
    }

  }
}
