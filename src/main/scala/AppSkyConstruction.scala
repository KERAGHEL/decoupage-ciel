import AppZone.partitionnementFichiers
import org.apache.spark.{SparkConf, SparkContext}

object AppSkyConstruction {
  def main (args: Array[String]): Unit = {
    val compte = "p1608626" // à modifier selon l'utilisateur
    if (args.length == 2) {
      val conf = new SparkConf().setAppName( "AppSkyConstruction" )
      val sc = new SparkContext( conf )
      val result = AppZone.skyConctrution(sc,args(0))
      val countZones =  AppZone.splitSky(result,128)
      AppZone.StockDtatOfZones(countZones,args(1),sc)
      sc.stop()
    }else {
      println("Usage: spark-submit --class SparkTPApp2 /home/" + compte + "/TPNote-assembly-1.0.jar " +
        "hdfs:///tp-data/Source " +
        "[hdfs:///user/" + compte + "/repertoire-resultat]")
    }
  }
}
