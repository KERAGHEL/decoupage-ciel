##### KERAGHEL ABDENACER p1608626
##### AMRINE Amine Mousaab p1813572

#### Projet spark

* L'objectif de ce projet est de decomposer un dataset representant les données astronomiques en zone suffisamment petites
dont le but est d'optimiser le temps de recherche du voisinnage d'un point donné

* Pour ce tp on a réussi de mettre en place les applications suivantes : \
    1- AppSkyConstruction : qui sert a appeler les fonctions qui font le necessaire pour calculer le ciel a partir des dataset et de le couper en petites zone qui seront stoquées dans un fichier\
    2- AppFilesPartition : qui sert a partitionner les fichiers a des paquets de 128 selon les zones

##### Fonctions implémentées (dans l'objet AppZone ):
 1 - zone englobante : qui englobe 2 zone données\
 2 - toZone : qui transforme un tuple a une zone  (minRa,maxRa,minDecl,maxDecl)\
 3 - skyConstruction : construit le ciel\
 4 - splitSky : decoupe le ciel en petites zones\
 5 - StockDtatOfZones : ici on stocke les informations sur les zones dans un fichier qu'on utilise comme catalogue\
 6 - LireCatalogue : c'est la fonction qui charge le catalogue\
 7 - pointAppartenance : ici on return une Map qui associe a chaque tuple des data la zone correspondante\
 8 - partitionnementFichiers : c'est une fonction qui sert a partitionner les data selon les zone
 9 - pointNeighboursZone : une fonction qui affiche dans quelle zone situent les voisins du point entree en paramètre

##### Execution des applications :
pour executer les applications soit AppSkyConstruction ou AppFilesPartition : \
    il faut lancer : <b>sbt assembly</b> sur la racine du projet \
    copier le TPNote-assembly.jar sur la vm \

##### pour construire le ciel il faut lancer :
    Usage: spark-submit --class SparkTPApp2 /home/" + compte + "/TPNote-assembly-1.0.jar " +
            "hdfs:///tp-data/Source " +
            "[hdfs:///user/" + compte + "/repertoire-resultat]
    
##### pour partitionner les fichiers et les écrire dans un repertoire :
    "Usage: spark-submit --class AppFilesPartition /home/" + compte + "/TPNote-assembly-1.0.jar " +
            "hdfs:///path-to-catalogue hdfs:///tp-data/Source"+
            "[hdfs:///user/" + compte + "/repertoire-resultat]"
            
#### Histogramme des resultats :
###### histogramme de taille de fichier par zone :
![](./histogrammes/tailleFichierParZone.png)

###### histogramme de nombre de ligne par zone
![](./histogrammes/nbLignesParZone.png)